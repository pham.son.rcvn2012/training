<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace' => 'Backend', 'prefix' => 'backend'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::get('/login', ['as' => 'backend.auth.login', 'uses' => 'LoginController@showLoginForm']);
        Route::post('/login', ['as' => 'backend.auth.login.post', 'uses' => 'LoginController@processLogin']);
        Route::get('/logout', ['as' => 'backend.auth.logout', 'middleware' => 'auth', 'uses' => 'LoginController@logout']);
       
    });
	    
    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'product'], function () {
	        Route::get('/', ['as' => 'backend.product.index', 'uses' => 'ProductController@getListProduct']);
	        Route::post('/change-status', ['as' => 'backend.product.changestatus', 'uses' => 'ProductController@updateStatusProduct']);
	        Route::get('/report', ['as' => 'backend.product.report', 'uses' => 'ProductController@getReportCsv']);
            Route::post('/import-csv', ['as' => 'backend.product.import', 'uses' => 'ProductController@importCsv']);
            Route::post('/import-csv-2', ['as' => 'backend.product.import.bigdata', 'uses' => 'ProductController@importBigCsv']);
            Route::post('/import-csv-3', ['as' => 'backend.product.import.bigdata2', 'uses' => 'ProductController@importFile']);
            Route::post('/export-csv', ['as' => 'backend.product.export', 'uses' => 'ProductController@exportCsv']);
            Route::get('/downloaderror/{keyCached}', ['as' => 'backend.product.download', 'uses' => 'ProductController@downloadErrorCsv']);
	    });
    });
});
