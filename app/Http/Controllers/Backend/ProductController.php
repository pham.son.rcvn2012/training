<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Helpers\ProductExportCsv;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Storage;
use File;
use Log;
use Cache;

class ProductController extends Controller
{
    /**
     * Get all data.
     *
     * @param $request Request
     * @return collection
     */

    public function getListProduct(Request $request) 
    {
        $limit = 50;

        $params = [
            'item'             => $limit,
            'page'             => $request->page ?? null,
            'cheetah_status_2' => $request->cheetah_status_2 ?? null,
            'cheetah_status_1' => $request->cheetah_status_1 ?? null,
            'cheetah_status_0' => $request->cheetah_status_0 ?? null,
            'product_jan'      => $request->product_jan ?? null,
            'maker_full_nm'    => $request->maker_full_nm ?? null,
            'product_code'     => $request->product_code ?? null,
            'product_name'     => $request->product_name ?? null
        ];

        $product = new Product;

        $arrListProduct = $product->getList($params, $limit);

        $pagination = $limit;

    	return view('backend.product.index')->with([
            'arrListProduct' => $arrListProduct,
            'params'         => $params,
            'pagination'     => $pagination
        ]);
    }

    /**
     * Update status product.
     *
     * @param $request Request
     */

    public function updateStatusProduct (Request $request) 
    {
        $data = $request->all();

        $type = $data['type_change_status'];

        try {
            DB::beginTransaction();

            $product = new Product;

            $product->updateStatusProductBE($data, $type);

            DB::commit();

            return redirect()->route('backend.product.index')->with(['messageSuccess' => 'Update Status Product Success !']);
            
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->route('backend.product.index')->with(['messageFail' => 'Update Status Product Fail !']);
        }
    }

    /**
     * load view report csv.
     *
     */
    public function getReportCsv () 
    {
        return view('backend.product.report');
    }

    /**
     * import file csv
     * @param file csv
     */
    public function importCsv (Request $request) 
    {
        $errors = "";
        $info = "";
        if($request->isMethod('post')){
            if ($request->hasFile('fileImport')) {
                $file = $request->fileImport;
                $exFile = $file->getClientOriginalExtension();
                if($exFile != "csv"){
                    $errors = "CSVファイルのフォーマットは正しくありません。正しいファイルを選択してください。";
                }else{
                    $destinationPath = storage_path("temp");
                    if(!file_exists($destinationPath)){
                        mkdir($destinationPath);
                    }
                    if(!($path = $file->move($destinationPath, $file->getClientOriginalName()))){
                        $errors = "Errors System.";
                    }else{
                        DB::beginTransaction();
                        try{

                            $file = fopen($path, "r");
                            $countRow = 0;

                            $price_supplier_id = Auth::user()->t_admin_id;
                            
                            while ( ($data = fgetcsv($file, 100000, ",")) !== FALSE ) {

                                if($countRow){
                                    $data = mb_convert_encoding($data, "UTF-8", "SJIS");

                                    $product_code = $data[2];
                                    
                                    if ($product_code == null) 
                                    {
                                        $errors = 'Product code is not blank.';
                                        DB::rollBack();
                                        break;
                                    }

                                    $product_info = Product::find($product_code);

                                    if (!empty($product_info)) 
                                    {
                                        $errors = 'Product '.$product_info->product_name.' is exists';
                                        DB::rollBack();
                                        break;
                                    }

                                    $insert = [
                                        'product_code'      => $product_code,
                                        'price_supplier_id' => $price_supplier_id,
                                        'product_name'      => $data[4] ?? null,
                                        'maker_full_nm'     => $data[1] ?? null,
                                        'product_jan'       => $data[3] ?? null,
                                        'cheetah_status'    => 0,
                                        'process_status'    => 0,
                                        'list_price'        => 0
                                    ];

                                    DB::table('mst_product_copy1')->insert($insert);
                                } 
                                $countRow++;
                            }
                            fclose($file);
                            if(empty($errors)){
                                DB::commit();
                                $info = "込作業完了データの取り込みは完了しました。";
                            }
                        }catch (\Exception $ex){
                            $errors = $ex->getMessage();
                            DB::rollBack();
                        }
                    }
                }
            } else {
                $errors = "CSVファイルを選択してください。";
            }
        }
        return redirect()->back()->with([
            "messageFail" => $errors,
            "messageSuccess" => $info
        ]);
    
    }

    /**
     * export file csv
     * @param request Request
     * @return data
     */

    public function exportCsv (Request $request) 
    {
        $csv = new ProductExportCsv(ProductExportCsv::EXPORT);
        $data = $csv->setData($request->all());

        $callback = function() use($csv) {
            $csv->csvContentStream();
        };

        @ob_clean();
        return Response::streamDownload($callback,$csv->getFileName(), $csv->responseHeader);
    }

    /**
     * Process import file: read content and parse
     * @param request
     * @return  json
     */
    public function importBigCSV(Request $request)
    {
        $nameCsv    = $request->input('name_csv', '');
        $fileUpload = $request->file('fileImport2');
        $pathUpload = "uploads";
        $msg = $this->upload($fileUpload, $nameCsv);
        if ($msg["flg"] === 0) {
            return response()->json($msg);
        }
        $fileName = $msg["msg"];
        
        //Get file from store
        $contentCsv = Storage::disk('local')->get($fileName);
        
        if ($contentCsv === "") {
            return response()->json([
                "process" => 'uploadCsv',
                "flg"     => 0,
                "msg"     => array("importFile2"=>__('csv empty content'))
            ]);
        }
        $breakCharacter = "\n";

        $readers      = str_getcsv($contentCsv, $breakCharacter);

        $total        = count($readers);
        $maxTotalRead = 1000;
        if ($total<=$maxTotalRead) {
            $maxTotalRead = $total;
        }
        $timeTotal = round($total/$maxTotalRead);
        $fileCorrect = date("YmdHis").'_correct.csv';
        Storage::disk('local')->put($fileCorrect, '');

        return response()->json([
                "process"      => 'uploadCsv',
                "flg"          => 1,
                "timeRun"      => $timeTotal,
                "currPage"     => 0,
                "total"        => $maxTotalRead,
                "filename"     => $fileName,
                "error"        => 0,
                "realFilename" => $fileUpload->getClientOriginalName(),
                "fileCorrect" => $fileCorrect,
            ]);
    }

    /**
     * Upload file to storage
     * @param   string  $fileUpload file upload
     * @return  array   contains success/unsuccess flag and msg
     */
    public function upload($fileUpload, $nameCsv)
    {
        $file = array('importFile' => $fileUpload);

        Validator::extend('mineCSV', function ($parameters, $value, $attribute) {
            $ext = $value->getClientOriginalExtension();
            if ($ext === "csv") {
                return true;
            } else {
                return false;
            }
        });

        // setting up rules
        $rules = array('importFile' => 'required|mineCSV'); 
        
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            Log::error($validator->errors());
            return array(
                "process" => 'uploadCsv',
                "flg"     => 0,
                "msg"     => $validator->errors()->toArray()['importFile'][0],
            );
        }
        if ($fileUpload->isValid()) {
            // getting image extension
            $extension = $fileUpload->getClientOriginalExtension(); 
            // renameing file
            $fileName = date("YmdHis").'.'.$extension; 
            Storage::disk('local')->put($fileName, File::get($fileUpload));
        }
        return array(
                "flg" => 1,
                "msg" => $fileName
            );
    }

    /**
     * process importFile data.
     * Return $object json
     */
    public function importFile(Request $request)
    {
        $fileName    = $request->input("filename", "");
        $timeRun     = $request->input("timeRun", 0);
        $currPage    = $request->input("currPage", 0);
        $total       = $request->input("total", 0);
        $countErr    = $request->input("count_err", 0);
        $realFile    = $request->input("realFilename", "");
        $fileCorrect = $request->input("fileCorrect", "");
        $flg         = false;

        $info        = $this->checkFileCsv($fileName);

        if ($info["flg"] === 0) 
        {
            return response()->json($info);
        } 
        else 
        {
            $readers = $info["msg"];
        }
        $table  = '';
        $arrCol = [];

        $flg = $this->processCsv(
            $readers,
            $currPage,
            $total,
            $fileName,
            (int) $countErr,
            $realFile,
            $fileCorrect
        );

        $table    = 'mst_product';
        $arrCol   = [
            'cheetah_status',
            'maker_full_nm',
            'product_code',
            'product_jan',
            'product_name',
            'list_price',
            'process_status',
        ];

        if ($currPage === $timeRun) 
        {
            $this->processCorrectCsv($fileCorrect, $arrCol, $table);
        }

        $arrProcess = array(
            "fileName"     => $fileName,
            "timeRun"      => $timeRun,
            "currPage"     => $currPage,
            "total"        => $total,
            "realFilename" => $realFile,
            "fileCorrect"  => $fileCorrect,
        );

        $info = $this->processDataRunCsv($flg, $arrProcess);

        return response()->json($info);
    }

    /**
     * Process import file correct
     * @param   string  $fileName
     * @param   string  $column
     * @param   string  $table
     * @return  string
     */
    public function processCorrectCsv($fileName, $column, $table)
    {
        $strColumn = implode(",", $column);
        $temp = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileName;
        $temp = str_replace("\\", '/', $temp);
        $query = <<<eof
        LOAD DATA LOW_PRIORITY LOCAL INFILE '$temp'
         REPLACE INTO TABLE $table
         CHARACTER SET sjis
         FIELDS TERMINATED BY ',' ENCLOSED BY '"'
         LINES TERMINATED BY '\r\n'
        ($strColumn, price_supplier_id);
    eof;
        try {
            DB::beginTransaction();

            $result = DB::connection()->getpdo()->exec($query);

            if ($result) 
            {
                DB::commit();
                return true;
            }
        } catch (\Exception $e) {
            DB::rollback();
            return;
        }
    }   

    /**
     * Check file and content of file
     * @param   request
     * @return  array       contains value of flag and msg
     */
    public function checkFileCsv($fileName) 
    {
        //Check file exist
        $existFile = Storage::disk('local')->exists($fileName);
        if ($existFile === false) 
        {
            return array(
                "flg" => 0,
                "msg" => 'csv not found',
            );
        }
        //Check content file
        $contentCsv = Storage::disk('local')->get($fileName);

        if ($contentCsv === "") {
            return array(
                "flg" => 0,
                "msg" => 'csv empty content',
            );
        }
        $fileContentUtf = $contentCsv;
        
        $fileContentUtf=  mb_convert_encoding($contentCsv, 'UTF-8', 'Shift-JIS');
        
        $breakCharacter = "\n";
        $readers = str_getcsv($fileContentUtf, $breakCharacter);
        return array(
                "flg" => 1,
                "msg" => $readers
            );
    }

        /**
     * Process data return for importing file
     *
     * @param   boolean $flg        flag for process function: true continue, false: stopt
     * @param   array   $arrProcess contains value of a processing
     * @return  array   contains value of flag and value of processing
     */
    public function processDataRunCsv($flg, $arrProcess)
    {
        $fileName     = $arrProcess["fileName"];
        $timeRun      = $arrProcess["timeRun"];
        $currPage     = $arrProcess["currPage"];
        $total        = $arrProcess["total"];
        $fileCorrect  = $arrProcess["fileCorrect"];
        $realFilename = !empty($arrProcess["realFilename"]) ? $arrProcess["realFilename"] : '';

        if ($flg === true) 
        {
            $nameCached  = explode(".", $fileName);
            $nameCached  = $nameCached[0];
            $nextPage = $currPage+1;
            $error = 0 ;
            if ($currPage === $timeRun) {
                //If finish process when current page = total time run
                //Check cached error
                if (Cache::has($nameCached)) 
                {
                    $error = 1 ;
                }
                //Return data
                return array(
                    "process"      => 'uploadCsv',
                    "flg"          => 0,
                    "msg"          => "Finish",
                    "timeRun"      => $timeRun,
                    "currPage"     => $currPage,
                    "total"        => $total,
                    "filename"     => $nameCached,
                    "error"        => $error,
                    "realFilename" => $realFilename,
                    "fileCorrect"  => $fileCorrect,
                );
            } 
            else 
            {
                return array(
                    "process"      => 'uploadCsv',
                    "flg"          => 1,
                    "timeRun"      => $timeRun,
                    "currPage"     => $nextPage,
                    "total"        => $total,
                    "filename"     => $fileName,
                    "realFilename" => $realFilename,
                    "fileCorrect"  => $fileCorrect,
                );
            }
        } 
        else 
        {
            //Return error when process is false
            return array(
                "process" => 'uploadCsv',
                "flg"     => 0,
                "msg"     => $flg
            );
        }
    }
    /**
     * Convert csv from cached file
     * @param   string  $keyCached   key of cached
     * @return  string
     */
    public function downloadErrorCsv($keyCached)
    {
        if (!Cache::has($keyCached)) 
        {
            return response("No data");
        }

        $value = Cache::get($keyCached);
        return response($value, 200)
              ->header('Content-Type', "text/csv")
              ->header('Content-Encoding', 'Shift-JIS')
              ->header('Content-Disposition', 'attachment; filename="error'.date("YmdHis").'.csv"');
    }

     /**
     * Process data in file csv for diy
     * @param   string  $readers file name in storage
     * @return  boolean
     */
    public function processCsv($arr, $offset, $length, $fileName, $countErr = 0, $realFile = "", $fileCorrect = "")
    {
        $readers = array_slice($arr, $offset*$length, $length);
        if ($readers === null) {
            return false;
        }
        $totalColumn = 7;
        $arrError = array();
        $num = 1;
        $checkHead = false;
        $checkFail = false;
        if ($countErr >= 3) 
        {
            if ($offset === '0') 
            {
                $checkHead = true;
            }
            $checkFail = true;
        }

        $url = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $fileCorrect;
        $file = fopen($url, 'a');
        
        foreach ($readers as $key => $row) 
        {
            if (empty($row) || ($offset === '0' && $num === 1)) 
            {
                $num++;
                continue;
            }
            
            $column   = str_getcsv($row, ",", "'");
            
            if (count($column) !== $totalColumn) 
            {
                $arrError[] = trim($row) . ('number column not match');
                continue;
            }
            
            $column[0] = (int)$column[0];
            $column[1] = '"' . $column[1]. '"';
            $column[4] = '"' . $column[4]. '"';
            $column[5] = (int)$column[5];
            $column[6] = (int)$column[6];

            if ($column[5] == '') 
            {
                $column[5] = 0;
            } 
            
            $column  = array_map('trim', $column);
            $arrTemp = $column;
            $arrTemp['price_supplier_id'] = Auth()->user()->t_admin_id;

            fputs($file, mb_convert_encoding(implode(",", $arrTemp), 'Shift-JIS', 'UTF-8') . "\r\n");
        }

        fclose($file);
        $nameCached  = explode(".", $fileName);
        $nameCached  = $nameCached[0];
        if (count($arrError) > 0) {
            if (!Cache::has($nameCached) && !$checkHead) {
                $header = array_slice($arr, 0, 1);
                $arrError = array_merge($header, $arrError);
            }
            mb_convert_variables('SJIS', 'UTF-8', $arrError);
            $this->updateCached($nameCached, implode("\r\n", $arrError));
        } elseif (Cache::has($nameCached)) {
            $this->updateCached($nameCached, '');
        }
        return true;
    }

    public function updateCached($key, $content)
    {
        $newContent = $content;
        $oldData = "";
        if (Cache::has($key)) 
        {
            $oldData = Cache::get($key);
            if (!empty($content)) 
            {
                $oldData .= "\r\n";
            }
        }
        $resultContent = $oldData.$newContent;
        return Cache::put($key, $resultContent, 600);
    }

}
