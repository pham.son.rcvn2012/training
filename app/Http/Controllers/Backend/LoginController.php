<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use DB;
use Cookie;

class LoginController extends Controller
{
	use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Where to redirect users after logout.
     *
     * @var string
     */
    protected $redirectAfterLogout;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:' . $this->guard)->except('logout');
        $this->redirectTo = route('backend.product.index');
        $this->redirectAfterLogout = route('backend.auth.login');
    }

    /**
     * get view login
     */
    public function showLoginForm() 
    {
    	if (Auth::check()) {
			return "Login Success !";
		}

    	return view('backend.auth.login');
    }

    /**
     * Process login .
     *
     * @param $request Request
     * @return view dashboarh
     */
    public function processLogin(LoginRequest $request) 
    {
    	// Authentication passed...

    	if (Auth::attempt([
    		'email' => $request->email,
    		'password' => $request->password ], 
    		$request->has('remember'))
		) 
    	{
    		DB::beginTransaction();
            
    		$request->session()->put('email',  $request->email);
    		$request->session()->put('password',  $request->password);

    		$user = User::where('email', $request->email)
    		->update([
    			'last_login_at' => Carbon::now(),
    			'last_login_ip' => $request->getClientIp(),
    			]);

    		DB::commit();
    		
    		return $this->sendLoginResponse($request);
        }

        return redirect()->route('backend.auth.login')->with('messageFail', 'Login Faller !');
    }

    /**
     * Logout  .
     *
     * @return view login
     */

    public function logout() 
    {
    	auth($this->guard)->logout();

    	return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : route('backend.auth.login'));
    }

    protected function sendLoginResponse(Request $request)
    {
        // set remember me expire time
        $rememberTokenExpireMinutes = ‭43200;

        // first we need to get the "remember me" cookie's key, this key is generate by laravel randomly
        // it looks like: remember_web_59ba36addc2b2f9401580f014c7f58ea4e30989d
        $rememberTokenName = Auth::getRecallerName();

        // reset that cookie's expire time
        Cookie::queue($rememberTokenName, Cookie::get($rememberTokenName), $rememberTokenExpireMinutes);

        // the code below is just copy from AuthenticatesUsers
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

}
