<?php

namespace App\Helpers;

use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProductExportCsv
{
    const EXPORT = "export";

    private $_fileName;
    private $_data;
    private $_headerCsv;
    public $responseHeader = array(
        "Content-type" => "text/csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
    );

    public function __construct ($action)
    {
        if ($action === self::EXPORT) {
            $this->_fileName = "請求データ_" . date('Ymd') . ".csv";
            $this->_headerCsv = [
                'ステータス' => 'cheetah_status',
                'メーカー名' => 'maker_full_nm',
                '品番' => 'product_code',
                'JANコード' => 'product_jan',
                '商品名' => 'product_name',
                '定価' => 'list_price',
                '処理ステータス' => 'process_status'
            ];
        }
    }

    /**
     * set data csv.
     * @param array
     * @return array
     */

    public function setData ($params) 
    {
        $product = new Product;

        $product = $product->getList($params, $params['limit']);

        $data = $product->toArray();

        $data = $this->formatDataExport($data['data']);

        $this->_data = $data;
    }

    /**
     * get csv content.
     */

    public function csvContentStream () {
        $data = $this->_data;
        $headerCsv = $this->_headerCsv;
        $file = fopen('php://output', 'w');
        fwrite($file, join(",",$this->encodeArray(array_keys($headerCsv),'CP932'))."\r\n");

        if (is_array($data)) {
            foreach ($data as $i => $row) {
                fwrite($file, join(",",$this->encodeArray($row,'CP932'))."\r\n");
            }
        }
        fclose($file);
    }

    /**
     * get file name csv.
     * @return file name
     */

    public function getFileName () {
        return $this->_fileName;
    }

    /**
     * encode array.
     *
     * @param array, format
     * @return array
     */

    public static function encodeArray($arr, $toEncode='SJIS') {
        return array_map( function($str) use ($toEncode) {
            return mb_convert_encoding (str_replace(array("\r\n","\n\r","\r"), "\n", $str), $toEncode);
        }, $arr);
    }

    /**
     * Format data export file csv.
     *
     * @param array
     * @return array
     */
    private function formatDataExport ($input = []) 
    {   
        $data = [];

        foreach ($input as $key => $value) {
            $data[$key] = [
                'cheetah_status' => $value['cheetah_status'],
                'maker_full_nm' => $value['maker_full_nm'],
                'product_code' => $value['product_code'],
                'product_jan' => $value['product_jan'],
                'product_name' => $value['product_name'],
                'list_price' => $value['list_price'],
                'process_status' => $value['process_status'],
            ];
        }

        return $data;
    }

}
