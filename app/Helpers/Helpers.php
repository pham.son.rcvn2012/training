<?php

if (!function_exists('pagination')) {

    /**
     * pagination
     *
     * @param object $arrData
     * @param string $pagination
     * @param int $item
     * @param string $position
     *
     * @return string
     */
    function pagination($arrData, $pagination, $item, $position)
    {
        return view('pagination', [
            'arrData' => $arrData,
            'pagination' => $pagination,
            'item' => $item,
            'position' => $position
        ]);
    }
}
