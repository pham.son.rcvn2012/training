<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    protected $table = 'mst_product';

    /**
    * The primary key.
    * @var string
    */
    protected $primaryKey = 'product_code';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_code', 'price_supplier_id', 'product_name', 'product_jan', 
        'maker_cd', 'list_price', 'product_maker_code', 'brand_name', 'maker_full_nm', 
        'cheetah_status', 'process_status', 'in_date', 'up_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
    * The type of primary key.
    * @var string
    */
    protected $keyType = 'string';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'in_date';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'up_date';

    /**
     * Get all data product
     *
     * @param $request Request
     * @return array
     */

    public function getList ($input = [], $limit = 0) 
    {
        $price_supplier_id = Auth::user()->t_admin_id;

        $query = $this->where('price_supplier_id', $price_supplier_id)
            ->orderby('product_code', 'ASC');

        if (!empty($input['cheetah_status_2']))
        {
            $query->where('cheetah_status', 2);
        }
        if (!empty($input['cheetah_status_1']))
        {
            $query->where('cheetah_status', 1);
        }
        if (!empty($input['cheetah_status_0']))
        {
            $query->where('cheetah_status', 0);
        }

        if (!empty($input['product_jan'])) 
        {   
            $query = $this->searchKey($query, $input['product_jan'], 'product_jan');
        } 

        if (!empty($input['maker_full_nm'])) 
        {
            $query = $this->searchKey($query, $input['maker_full_nm'], 'maker_full_nm');
        }

        if (!empty($input['product_code'])) 
        {
            $query = $this->searchKey($query, $input['product_code'], 'product_code');
        }

        if (!empty($input['product_name'])) 
        {
            $query = $this->searchKey($query, $input['product_name'], 'product_name');
        }

        $arrListProduct = $query->paginate($limit);

        return $arrListProduct;
    }

    /**
     * search key data.
     *
     * @param $query sql
     * @return query sql
     */

    private function searchKey($query, $input, $name) 
    {
        $data = explode(' ', $input);
        foreach ($data as $key => $value) {
            if ($key < 1) 
            {
                $query->where($name, "like", '%'.$value.'%');
            }
            else {
                $query->orWhere($name, "like", '%'.$value.'%');
            }
        }

        return $query;
    }

    /**
     * Update status product.
     *
     * @param $data , type update
     */

    public function updateStatusProductBE ($data = [], $type) 
    {
        switch ($type) 
        {
            case '1':
                foreach ($data['product_code'] as $key => $value) {
                    $this->updateStatus($value, 2);
                }
                break;
            case '2':
                foreach ($data['product_code'] as $key => $value) {
                    $this->updateStatus($value, 1);
                }
                break;
            default:
                foreach ($data['product_code'] as $key => $value) {
                    $this->updateStatus($value, 0);
                }
                break;
        }

        return;
    }

    /**
     * Update status product.
     *
     * @param $product code , type status
     */

    private function updateStatus($product_code, $type_status) 
    {
        $this->where('product_code', $product_code)
            ->update([
                'cheetah_status' => $type_status,
                'process_status' => 1
            ]);

        return;
    }

    /**
     * Import file csv.
     *
     * @param file csv
     */

}
