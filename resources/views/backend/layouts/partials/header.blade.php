<button type="button" class="sidebar-toggle">
    <span class="toggle-icon"></span>
</button>
<div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="">
                <img src="{{ asset('static/images/avatar.png') }}" class="rounded-circle" />
                <span>{{ auth()->user()->fullname }}</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a href="">
                        <i class="fas fa-user"></i>
                        <span>Profile</span>
                    </a>
                </li>
                <!--<li class="divider" role="separator"></li>//-->
                <li>
                    <a href="{!! route('backend.auth.logout') !!}">
                        <i class="fas fa-power-off"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
