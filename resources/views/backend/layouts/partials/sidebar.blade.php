<div class="sidebar-logo">
    <a href="{!! route('backend.product.index') !!}" class="logo"></a>
</div>
<ul class="sidebar-menu">
    <li class="menu-item">
        <a href="{!! route('backend.product.index') !!}" class="menu-link">
            <span class="menu-icon">
                <i class="fas fa-users"></i>
            </span>
            <span class="menu-title">Manage Product</span>
        </a>
    </li>
</ul>
<div class="content-mask"></div>
