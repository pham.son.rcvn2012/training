<div class="modal fade" id="myModal" role="dialog"  >
    <form action="{!! route('backend.product.changestatus') !!}" method="post" id="changeStatus" >
        {{ csrf_field() }}
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content" style="width:130%;right: 15%">
                <div class="modal-header">
                    <h4 class="modal-title">Change Status</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label class="form-label" style="background-color: #337ab7; color: #fff;padding-left: 16px;padding-right: 16px; ">廃番 </label> <span>下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</span>
                    <br>
                    <label class="form-label" style="background-color:#f0ad4e; color: #fff;padding-left: 10px;padding-right: 10px; ">閉じる </label> <span>廃番にしない場合は、閉じる ボタンを押してください。</span>
                    <br>
                    <div id="show-product-code"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
                    <button type="submit" class="btn btn-primary" id="submit_modal">確定</button>
                </div>
            </div>
        </div>
    </form>
        
</div>