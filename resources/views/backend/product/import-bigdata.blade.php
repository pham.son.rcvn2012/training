<div class="modal fade" role="dialog" id="importBigCsv" >

    <form name="frm-search" enctype="multipart/form-data" id="form-csv">

        <input type="hidden" id="action-modal" value="" name="name_csv">
        {{ csrf_field() }}
        <div class="modal-dialog">

            <div class="modal-content" style="width:130%;right: 15%">
                
                <div class="modal-header">
                    <h4 class="modal-title">Import Big CSV Product</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body col-12" style="left: 20%;">
                    
                    <div class="controls form-group col-6" >   
                        <input class="form-control" type="text" name="fileImport2" id="fileImportInput2" value="" readonly="" />
                    </div>
                    <div class="col-2">
                        <label >
                            <span class="btn btn-primary">参照 <input id="fileImport2" style="display: none" type="file" name="fileImport2">
                            </span>
                        </label>
                    </div>
                    <div id="Link-Error" style="color: red;padding-top: 10px;white-space: nowrap;">
                        <a></a>
                    </div>
                    <div id="Msg-error" style="color: red;padding-top: 10px;white-space: nowrap;"></div>
                    <div id="show-product-code"></div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <span class="" role="status" aria-hidden="true" id="loading"></span>
                    <button class="btn btn-success " type="submit" id="submit_modal"><i class="fas fa-file-csv"></i> Import CSV</button>
                </div>
            </div>
        </div>
    </form>

</div>