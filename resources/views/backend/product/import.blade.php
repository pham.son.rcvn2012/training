<div class="modal fade" role="dialog" id="importCsv" >
    <form name="frm-search" action="{!! route('backend.product.import') !!}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-dialog">
            <div class="modal-content" style="width:130%;right: 15%">
                <div class="modal-header">
                    <h4 class="modal-title">Import CSV Product</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body col-12" style="left: 20%;">
                    
                    <div class="controls form-group col-6" >   
                        <input class="form-control" type="text" name="fileImport" id="fileImportInput" value="" readonly="" />
                        
                    </div>
                    <div class="col-2">
                        <label >
                            <span class="btn btn-primary">参照 <input id="fileImport" style="display: none" type="file" name="fileImport">
                            </span>
                        </label>
                    </div>
                    <br>
                    <div id="show-product-code"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success " type="submit" id="submit_modal"><i class="fas fa-file-csv"></i> Import CSV</button>
                </div>
            </div>

        </div>
    </form>
</div>