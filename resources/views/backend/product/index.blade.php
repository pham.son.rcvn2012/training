@extends('backend.layouts.layout')

@section('content')
<div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-list"></i> Product</h3>
    <div class="invalid-feedback" style="color: red;padding-top: 10px;white-space: nowrap;">111</div>
</div>
<div id="Msg" style="color: red;padding-top: 10px;white-space: nowrap;"></div>
@if ( $messageSuccess = Session::get('messageSuccess'))
    <div style="color: red;padding-top: 10px;white-space: nowrap;">{!! $messageSuccess !!}</div>
@endif

@if ( $messageFail = Session::get('messageFail'))
    <div  style="color: #0000FF;padding-top: 10px;white-space: nowrap;">{!! $messageFail !!}</div>
@endif
<hr>
<form id="frm-search" name="frm-search" action="{!! route('backend.product.index') !!}" method="get">
    <div class="card mb20 bg-light">
        <div class="card-body">
            <div class="form-group form-row">
                <div class="form-group" style="padding: 10px">
                    <input type="checkbox" value="2" name="cheetah_status_2" id="cheetah_status" 
                    @if($params['cheetah_status_2'])
                        checked="" 
                    @endif
                    >
                    <label class="form-label" >廃番 </label>
                </div>
                <div class="form-group" style="padding: 10px">
                    <input type="checkbox" value="1" name="cheetah_status_1" id="cheetah_status"
                    @if($params['cheetah_status_1'])
                        checked="" 
                    @endif
                    >
                    <label class="form-label">欠品 </label>
                </div>
                <div class="form-group" style="padding: 10px">
                    <input type="checkbox" value="3" name="cheetah_status_0" id="cheetah_status"
                    @if($params['cheetah_status_0'])
                        checked="" 
                    @endif
                    >
                    <label class="form-label">販売 </label>
                </div>
                <br>
                <div class="form-group" style="padding: 7px">
                    <label class="form-label">JANコード </label>
                    <input type="text"  value="{!! old('product_jan', $params['product_jan']) !!}" name="product_jan" id="product_jan" >
                </div>
                <div class="form-group" style="padding: 7px">
                    <label class="form-label">メーカー名 </label>
                    <input type="text"  value="{!! old('maker_full_nm', $params['maker_full_nm']) !!}" name="maker_full_nm" id="maker_full_nm" >
                </div>
                <div class="form-group" style="padding: 7px">
                    <label class="form-label">品番 </label>
                    <input type="text"  value="{!! old('product_code', $params['product_code']) !!}" name="product_code" id="product_code" >
                </div>
                <div class="form-group" style="padding: 7px">
                    <label class="form-label">商品名</label>
                    <input type="text"  value="{!! old('product_name', $params['product_name']) !!}" name="product_name" id="product_name" >
                </div>
                <div class="col-2 col-lg-2 text-right">
                    <button type="submit" class="btn btn-primary" id="search" style="width: 66%;white-space: nowrap;" >検索する</button>
                </div>
                <div class="col-2 col-lg-2 text-right">
                    <button type="reset" class="btn btn-primary" style="width: 66%;white-space: nowrap;" >元に戻す</button>
                </div>
            </div>
            
            <div class="form-group form-row">
                <div class="col-2 text-right">
                    <button type="button" class="btn btn-danger" style="width: 66%;white-space: nowrap;" data-toggle="modal" data-target="#myModal" id="btn-changestatus-3">廃番</button>
                </div>
                <div class="col-2 text-right">
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" id="btn-changestatus-2" style="white-space: nowrap;">欠品にする</button>
                </div>
                <div class="col-2 text-right">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" id="btn-changestatus" style="white-space: nowrap;">販売にする</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="card-body">
    <div class="form-group form-row" style="float: right;">
        <div style="padding-right: 10px">
            <form action="{!! route('backend.product.export') !!}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @if($params['cheetah_status_2'])
                <input type="hidden" value="2" name="cheetah_status_2" id="cheetah_status">
                @endif
                @if($params['cheetah_status_1'])
                <input type="hidden" value="1" name="cheetah_status_1" id="cheetah_status">
                @endif
                @if($params['cheetah_status_0'])
                <input type="hidden" value="3" name="cheetah_status_0" id="cheetah_status">
                @endif
                <input type="hidden"  value="{!! old('product_jan', $params['product_jan']) !!}" name="product_jan" id="product_jan" >
                <input type="hidden"  value="{!! old('maker_full_nm', $params['maker_full_nm']) !!}" name="maker_full_nm" id="maker_full_nm" >
                <input type="hidden"  value="{!! old('product_code', $params['product_code']) !!}" name="product_code" id="product_code" >
                <input type="hidden"  value="{!! old('product_name', $params['product_name']) !!}" name="product_name" id="product_name" >
                <input type="hidden"  value="{!! old('page', $params['page']) !!}" name="page" id="page" >
                <input type="hidden"  value="{!! old('item', $params['item']) !!}" name="limit" id="limit" >
                <div class="text-right">
                    <button type="submit" class="btn btn-secondary"><i class="fas fa-file-csv"></i> Export CSV</button>
                </div>
            </form>
        </div>
        <div style="padding-right: 10px">
            <button class="btn btn-success" data-toggle="modal" data-target="#importBigCsv" id="importBigCsv-btn"><i class="fas fa-file-csv"></i> Import big CSV</button>
        </div>
        <div >
            <button class="btn btn-success" data-toggle="modal" data-target="#importCsv" id="importCsv-btn"><i class="fas fa-file-csv"></i> Import CSV</button>
        </div>
        
    </div>
</div>
<div class="table-responsive" id="list-product">
    <table class="table table-striped table-bordered">
        <thead class="thead-light">
            <tr>
                <td colspan="8">{!! pagination($arrListProduct, $pagination, $params['item'], 'top') !!}</td>
            </tr>
            <tr>
                <th class="text-center w100px">ステータス</th>
                <th class="text-center w100px">メーカー名</th>
                <th class="text-center w100px">品番</th>
                <th class="text-center w100px">JANコード</th>
                <th class="text-center w200px">商品名</th>
                <th class="text-center w100px">定価</th>
                <th class="text-center w100px">処理ステータス</th>
                <th class="text-center w100px">出荷指示<br>+全て選択<br>-選択解除</th>
            </tr>
        </thead>
        <tbody>
            @foreach( $arrListProduct as $key => $value)
            <tr>
                <td class="text-center">{!! $value->cheetah_status !!}</td>
                <td class="text-center">{!! $value->maker_full_nm !!}</td>
                <td class="text-center">{!! $value->product_code !!}</td>
                <td class="text-center">{!! $value->product_jan !!}</td>
                <td class="text-center">{!! $value->product_name !!}</td>
                <td class="text-center">{!! $value->list_price !!}</td>
                <td class="text-center">{!! $value->process_status !!}</td>
                <td class="text-center"><input type="checkbox" name="cheetah_status" id="cheetah_status" value="{{$value->product_code}}"></td>
            </tr>
            @endforeach
        </tbody>
       <tfoot>
            <tr>
                <td colspan="8">{!! pagination($arrListProduct, $pagination, $params['item'], 'bottom') !!}</td>
            </tr>
        </tfoot>
    </table>
</div>

@include('backend.product.change-status')
@include('backend.product.import')
@include('backend.product.import-bigdata')

@stop

@section('javascript')
<!-- js link here -->
<script type="text/javascript">
    $("#changeStatus").on("submit", function(){
        return confirm("Do you want to change status the data?");
    });

    $("#btn-changestatus").click(function(){
        $.each($("input[name='cheetah_status']:checked"), function(){            
            $('div#show-product-code').append('<input type="text" name="product_code[]" value="'+$(this).val()+'" class="col-2"  readonly style="margin: 10px;">');
        });
        $('div#show-product-code').append('<input type="hidden" name="type_change_status" value="1" class="col-2"  readonly style="margin: 10px;">');
    });

    $("#btn-changestatus-2").click(function(){
        $.each($("input[name='cheetah_status']:checked"), function(){            
            $('div#show-product-code').append('<input type="text" name="product_code[]" value="'+$(this).val()+'" class="col-2"  readonly style="margin: 10px;">');
        });
        $('div#show-product-code').append('<input type="hidden" name="type_change_status" value="2" class="col-2"  readonly style="margin: 10px;">');
    });

    $("#btn-changestatus-3").click(function(){
        $.each($("input[name='cheetah_status']:checked"), function(){            
            $('div#show-product-code').append('<input type="text" name="product_code[]" value="'+$(this).val()+'" class="col-2"  readonly style="margin: 10px;">');
        });
        $('div#show-product-code').append('<input type="hidden" name="type_change_status" value="3" class="col-2"  readonly style="margin: 10px;">');
    });
    $("#fileImportInput").click(function () {
        $('#fileImport').trigger("click");
    });
    $('#fileImport').change(function(){
        var input = this;
        var url = $(this).val();
        $("#fileImportInput").val(url);
    });

    $("#fileImportInput2").click(function () {
        $('#fileImport2').trigger("click");
    });
    $('#fileImport2').change(function(){
        var input = this;
        var url = $(this).val();
        $("#fileImportInput2").val(url);
    });
    $('#form-csv').submit(function(event) {
        $('#loading').addClass('spinner-border spinner-border-sm');
        event.preventDefault();
        let file = $('input[name=fileImportInput]').val();
        $.ajax({
            url:"{{action('Backend\ProductController@importBigCsv')}}",
            method:"POST",
            dataType:'JSON',
            contentType: false,
            data: new FormData($(this)[0]),
            processData: false,
            success:function(data) {
                if (data.flg === 0) {
                    return;
                }
                processUploadFile(data);
            }
       })

    });

    function processUploadFile (data) {
        let formData   = new FormData();
        Object.keys(data).map((item) => {
            formData.append(item, data[item]);
        });
        formData.append('type', $('#action-modal').val());
        formData.append('_token', $('input[name=_token]').val());
        $.ajax({
            url:"{{action('Backend\ProductController@importFile')}}",
            method:"POST",
            dataType:'JSON',
            contentType: false,
            data: formData,
            processData: false,
            success:function(data) {
                if (data.flg === 0) {
                    if (data.msg === 'Finish') {
                        $('#loading').removeClass('spinner-border spinner-border-sm');
                        if (data.error == 0) {
                            $(('#importBigCsv')).modal('hide');
                            $('#Msg').append('Import File CSV Success !');
                        } else {
                            $("#Link-Error a").append('DownLoad File Error');
                            $("#Link-Error a").attr("href",'/training/public/backend/product/downloaderror/' + data.filename);
                            $('#Msg-error').append('File CSV Import Fail !');
                            
                        }
                    }
                } else {
                    processUploadFile(data);
                }
            }
       })
    }

</script>
@stop
