@extends('backend.layouts.auth')

@section('css')
<!-- css link here -->
@stop 

@section('content')
<form action="{{ route('backend.auth.login.post') }}" method="post" id="frmLogin" name="frmLogin">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="form-label form-required" for="email">Email</label>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Please enter your email" name="email" id="email" value="{{ old('email') }}"
            />
            <div class="input-group-append">
                <div class="input-group-text">
                    <i class="fas fa-envelope"></i>
                </div>
            </div>
        </div>
        @if ($errors->has('email'))
            <span class="error" style="color: yellow">{{ $errors->first('email') }}</span>
        @endif
    </div>
    <div class="form-group">
        <label class="form-label form-required" for="password">Password</label>
        <div class="input-group">
            <input type="password" class="form-control" placeholder="Please enter your password" name="password" id="password" value="{{ old('password') }}"
            />
            <div class="input-group-append">
                <div class="input-group-text">
                    <i class="fas fa-lock"></i>
                </div>
            </div>
        </div>
        @if ($errors->has('password'))
            <span class="error" style="color: yellow">{{ $errors->first('password') }}</span>
        @endif
    </div>
    @if ($msg = Session::get('messageFail'))
        <span class="error" style="color: yellow">{{ $msg }}</span>
    @endif
    <div class="form-group row">
        <div class="col-6 form-check mt-2">
            <input type="checkbox" id="remember" name="remember" value="1">
            <label class="form-check-label" for="remember">Remember</label>
        </div>
        <div class="col-6">
            <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fas fa-sign-in-alt"></i> Login</button>
        </div>
    </div>
    
</form>

@stop 
@section('javascript')
<!-- js link here -->
@stop