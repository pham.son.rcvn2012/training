<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <title>{!! config('site.general.site_name') !!} - @yield('title')</title>
        <meta charset="utf-8">
        <meta name="robots" content="noindex,nofollow" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
        <link rel="stylesheet" href="{{ asset('static/css/app.css') }}">
    </head>
    <body class="app-error">
        <div class="error-wrapper">
            <div class="error-content-wrapper">
                <div class="error-content">
                    <div class="error-code">
                        @yield('code', __('Oh no'))
                    </div>
                    <div class="error-line"></div>
                    <p class="error-message">
                        @yield('message')
                    </p>
                    <div>
                        <a href="" class="btn btn-primary">{{ __('Go Home') }}</a>
                    </div>
                </div>
            </div>
            <div class="error-image">
                @yield('image')
            </div>
        </div>
    </body>
</html>
