<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mst_users')->insert([
            'email' => 'phamson@gmail.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
